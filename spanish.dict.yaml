---
name: spanish
version: "1"
sort: original
...

# La Real Academia Española recomienda usar el caracter - como guión
# (U+002D, HYPHEN-MINUS). Ver https://www.rae.es/dpd/guion . Este
# caracter no es definido en este diccionario porque la mayoría de
# teclados españoles o los teclados ingleses soporta insertar este
# caracter.

# Vocales

á	'a
Á	'A

é	'e
É	'E

í	'i
Í	'I

ó	'o
Ó	'O

ú	'u
Ú	'U
# pingüino
# lingüística
# ambigüedad
ü	:u
Ü	:U
# Köhler (apellido)
ö	:o
Ö	:O

# Consonants

ñ	~n
Ñ	~N

# Punctuation marks

¿	~?
¡	~!

# Quotation marks
#
# The Royal Spanish recomends giving «» the highest precedence. See
# https://www.rae.es/dpd/comillas
«	<<
»	>>

# En https://www.rae.es/dpd/comillas, se menciona lo siguiente:
#
# #+BEGIN_QUOTE
# En los textos impresos, se recomienda utilizar en primera instancia
# las comillas angulares, reservando los otros tipos para cuando deban
# entrecomillarse partes de un texto ya entrecomillado. En este caso,
# las comillas simples se emplearán en último lugar: «Antonio me dijo:
# "Vaya 'cacharro' que se ha comprado Julián"».
# #+END_QUOTE
#
# A pesar que en este extracto la Real Academia Española usa "
# (U+0022, QUOTATION MARK) y no usa “ (U+201C, LEFT DOUBLE QUOTATION
# MARK) ” (U+201D, RIGHT DOUBLE QUOTATION MARK). En algunos textos en
# español, se podrían usar estos dos últimos, por lo que debe haber
# una manera de tipear esos caracteres.
“	<"
”	>"

# El método de entrada debe poder insertar las comillas porque según
# https://www.rae.es/dpd/comillas (2024-07-02T04:09:09+0000) las
# comillas pueden ser usadas:
#
# #+BEGIN_QUOTE
# f) En obras de carácter lingüístico, las comillas simples se utilizan
# para enmarcar los significados: La voz apicultura está formada a
# partir de los términos latinos apis 'abeja' y cultura 'cultivo,
# crianza'.
# #+END_QUOTE
#
# En el extracto mostrado arriba, solo se usa ' (U+0027, APOSTROPHE),
# no se usa ‘ (U+2018, LEFT SINGLE QUOTATION MARK) ni ’ (U+2019, RIGHT
# SINGLE QUOTATION MARK), pero debido que la RAE recomienda su uso,
# algunas obras escritas en español pueden usar U+2018 y U+2019, por
# lo tanto el método de entrada debe insertar esos caracteres.
‘	<'
’	>'

# El método de entrada debe poder insertar ― (U+2015, HORIZONTAL BAR)
# porque en https://www.rae.es/dpd/raya este caracter es usado.
#
# #+BEGIN_QUOTE
# 1. Signo de puntuación representado por un trazo horizontal (―) de
# mayor longitud que el correspondiente al guion (-) y al signo menos
# (−), con los que no debe confundirse. Puede emplearse como signo
# doble o simple.
#
# 2.3. En textos narrativos, la raya se utiliza también para
# introducir o enmarcar los comentarios y precisiones del narrador a
# las intervenciones de los personajes.
# #+END_QUOTE
―	__

# Él método de entrada debe poder insertar … (U+2026, HORIZONTAL
# ELLIPSIS) porque en https://www.rae.es/dpd/puntos%20suspensivos esté
# caracter es mencionado:
#
# #+BEGIN_QUOTE
# 1. Signo de puntuación formado por tres puntos consecutivos (…) ―y
# solo tres―, llamado así porque entre sus usos principales está el de
# dejar en suspenso el discurso.
# #+END_QUOTE
#
# No podemos usar la combinación de teclas "_." porque "." es usado
# por defecto en RIME para visitar la siguiente página de candidatos.
…	_:

# Miscellanous

º	_o
ª	_a
